/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myshell;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JTextArea;

/**
 *
 * @author czarate
 */
public class Processor {
    
    public Processor(JTextArea txtPrompt){
        this.txtPrompt=txtPrompt;
        newLine();
    }
    
    public void process(String command){
        history.add(command);
        if(!command.equals("")){
            String res=validOrder(command);
            if(res.equals("")){
                execute(command);
            }else{
                txtPrompt.append(res);
            }
        }
        newLine();
    }
    
    private void execute(String command){
        String[] parts=command.split(" ");
        if(parts[0].equals("clear")){
            txtPrompt.setText("");
        }
        if(parts[0].equals("exit")){
            System.exit(0);
        }
        if(parts[0].equals("pwd")){
            txtPrompt.append(url+"\n");
        }
        if(parts[0].equals("history")){
            if(history.size()>10){
                for(String line:history.subList(history.size()-10,history.size())){
                    txtPrompt.append(line+"\n");
                }
            }else{
                for(String line:history){
                    txtPrompt.append(line+"\n");
                }
            }
        }
        if(parts[0].equals("mkdir")){
            File fdest=new File(file.getAbsolutePath()+"/"+parts[1]);
            if(!fdest.exists()){
                fdest.mkdir();
            }else{
                txtPrompt.append("The specified directory already exists.\n");
            }            
        }
        if(parts[0].equals("edit")){
            File archivo=new File(file.getAbsolutePath()+"/"+parts[1]);
            Edit edit=new Edit(archivo);
            edit.setVisible(true);
        }
        if(parts[0].equals("cd")){
            if(parts[1].equals(".")){
                //se mantiene en el mismo directorio
                //nada que hacer ya estamos en el directorio
            }else{
                if(parts[1].equals("..")){
                    //procedimiento para ir un directorio arriba
                    if(nivel>0){
                        //reduciendo la url
                        StringTokenizer st=new StringTokenizer(url,"/");
                        url="/";
                        while(st.hasMoreTokens()){
                            String directory=st.nextToken();
                            if(st.countTokens()>0){
                                url+=directory+"/";
                            }
                        }
                        //moviendo el apuntador del archivo
                        file=new File(file.getAbsoluteFile().getPath().substring(0,file.getAbsoluteFile().getPath().lastIndexOf("\\"))+"\\");
                        nivel--;
                    }else{
                        txtPrompt.append("This is the root directory.\n");
                    }
                }else{
                    if(parts[1].equals("/")){
                        nivel=0;
                        url="/";
                        file=new File("filesystem");
                    }else{
                        File fdest=new File(file.getAbsolutePath()+"/"+parts[1]);
                        if(fdest.exists()){
                            url+=parts[1]+"/";
                            file=new File(file.getAbsolutePath()+"/"+parts[1]);
                            nivel++;
                        }else{
                            txtPrompt.append("Can't find the path specified.\n");
                        }
                    }
                }
            }
        }
        if(parts[0].equals("rm")){
            File fdest=new File(file.getAbsolutePath()+"/"+parts[1]);
            if(fdest.exists()){
                if(parts.length==2){
                    if(fdest.isDirectory()){
                        txtPrompt.append("This is a directory.\n");
                    }else{
                        fdest.delete();
                    }
                }else{
                    delete(fdest);
                }
            }else{
                txtPrompt.append("The specified directory does not exist.\n");
            }
        }
        if(parts[0].equals("ls")){
            if(parts.length==1){
                for(String f:file.list()){
                    txtPrompt.append(f+"\n");
                }
            }
            if(parts.length==2){
                if(parts[1].equals("-l")){
                    for(File f:file.listFiles()){
                        txtPrompt.append(f.getName()+"\t"+new java.text.SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(new java.util.Date(f.lastModified()))+"\n");
                    }
                }else{
                    for(File f:file.listFiles()){
                        try{
                            BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
                            AclFileAttributeView view = Files.getFileAttributeView(f.toPath(), AclFileAttributeView.class);
                            txtPrompt.append(f.getName()+"\t"+view.getOwner().getName()+"\t\n");
                        }catch(java.io.IOException e){
                            txtPrompt.append("Can't read file properties.\n");
                        }
                    }
                }
            }
            if(parts.length==3){
                for(File f:file.listFiles()){
                    try{
                        BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
                        AclFileAttributeView view = Files.getFileAttributeView(f.toPath(), AclFileAttributeView.class);
                        txtPrompt.append(f.getName()+"\t"+new java.text.SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(new java.util.Date(f.lastModified()))+"\t"+view.getOwner().getName()+"\n");
                    }catch(java.io.IOException e){
                        txtPrompt.append("Can't read file properties.\n");
                    }
                }
            }
        }
    }    
    
    private void delete(File f){ 
        if(f.isDirectory()){ 
            for(File c:f.listFiles()){ 
                delete(c); 
            }
        } 
        f.delete();
    } 
    
    private String validOrder(String command){
        String[] parts=command.split(" ");
        if(parts[0].equals("edit")){
            if(parts.length!=2){
                return "wrong number of parameters.\n";
            }
            return "";
        }
        if(parts[0].equals("rm")){
            if(parts.length<2||parts.length>3){
                return "wrong number of parameters.\n";
            }
            if(parts.length>2){
                if(!parts[2].equals("-r")){
                    return "Unknown parameter ["+parts[2]+"].\n";
                }
            }
            return "";
        }
        if(parts[0].equals("mkdir")){
            if(parts.length!=2){
                return "wrong number of parameters.\n";
            }
            return "";
        }
        if(parts[0].equals("ls")){
            if(parts.length<1||parts.length>3){
                return "wrong number of parameters.\n";
            }
            if(parts.length>1){
                if(!parts[1].equals("-l")&&!parts[1].equals("-t")){
                    return "Unknown parameter ["+parts[1]+"].\n";
                }
                if(parts.length>2){
                    if(!parts[2].equals("-l")&&!parts[2].equals("-t")){
                        return "Unknown parameter ["+parts[2]+"].\n";
                    }
                    if(parts[1].equals(parts[2])){
                        return "Use different parameters.\n";
                    }
                }
            }
            return "";
        }
        if(parts[0].equals("cd")){
            if(parts.length!=2){
                return "wrong number of parameters.\n";
            }
            return "";
        }
        if(parts[0].equals("pwd")){
            if(parts.length>1){
                return "wrong number of parameters.\n";
            }
            return "";
        }
        if(parts[0].equals("clear")){
            if(parts.length>1){
                return "wrong number of parameters.\n";
            }
            return "";
        }
        if(parts[0].equals("exit")){
            if(parts.length>1){
                return "wrong number of parameters.\n";
            }
            return "";
        }
        if(parts[0].equals("history")){
            if(parts.length>1){
                return "wrong number of parameters.\n";
            }
            return "";
        }
        return "Unknow command.\n";
    }
    
    private void newLine(){
        txtPrompt.append(url+"$>");
        txtPrompt.setCaretPosition(txtPrompt.getDocument().getLength());
    }
    
    private int nivel=0;
    private String url="/";
    private File file=new File("filesystem");
    private JTextArea txtPrompt;
    private ArrayList<String> history = new <String>ArrayList();
}
